***************
Hardware Errata
***************

Revision 2
==========

1. Fixed

2. Fixed

3. A new voltage limit circuit has been introduced. Can be bypassed
   to previous behavior by closing JP102

Revision 1
==========

1. A pull down resistor for I2C_EN signal of Si4688 sub-circuit is missing. As 
   a result the gates of Q301 are floating and might unexpectedly 
   raise the RESET signal through resistor divider RN301/R302 above threshold
   such that Si4688 leaves standby mode.
   This can be solved by connecting a 10k Resistor between GND and Pin 4 of 
   PCAL6408. (Most esily done using C105 GND pad)

2. Unfortunately the footprint of RN101 does not fit well with a 4x0603 resistor
   array. As a workaround, either solder four thin wires as needed or try to
   use two 2x0402 Convex resistor arrays

3. When using the Board with IO Voltage greater 3.7V, an increased current consumption
   on IO supply rail can be observed. The problem occurs since the IO voltage is
   connected directly with DVDDS1 of MAX98089 which support atmost 3.7V under normal
   conditions.

