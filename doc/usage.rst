*****
Usage
*****

I2C Bus interface
=================

I2C is the main interface to manage and control the shield. The
shield comes with integrated voltage level translation and can run
at any I/O Voltage (IOREF pin) between 1.8 and 5.5 V.

The following I2C bus addresses are used:

=======  =========  ========  ==============================================
Address  Reference  Device    Description
=======  =========  ========  ==============================================
0x10     U201       MAX98089  Audio Codec with integrated Class D amplifier
0x20     U102       PCAL6408  IO Expander to control various board functions
0x50     U103       24CW1280  128 kBit EEPROM for application use
0x64     U301       SI4688    DAB+/FM radio receiver chip.
=======  =========  ========  ==============================================

SI4688 usage
============

I2C Interface
-------------

Since the SI4688 pulls the I2C lines to GND during RESET, the
SI4688's I2C interface is disconnected on the shield
by default. In order to communicate with it, it must be connected 
to I2C bus by setting the P0 of U102 to High level:

- before setting P0 of U102 to High level, make sure to de-assert reset 
  signal of SI4688 by setting P1 of U102 to high level and delay some 
  milliseconds. 
- when asserting reset signal of SI4688 by setting P1 of U102
  to low level, make sure to disconnect I2C beforehand or at the same 
  time by setting P0 of U102 to low level

.. warning::

   Do not set P0 to High level while SI4688 is in reset state. Otherwise
   the I2C bus will be blocked forever.

Firmware flash organization
---------------------------

The SI4688 receiver chip U301 is connected to a 2MByte flash 
memory U302 which contains the firmware for SI4688. The 
following partitioning scheme is used for this flash
memory:

========  ============  =====================
Offset    Maximum Size  Description
========  ============  =====================
0x000000    16k         Spare
0x004000    16k         Romloader patch
0x008000   608k         FM Firmware
0x0a0000  1408k         DAB Firmware
========  ============  =====================



