ESP 32 based radio clock
========================

.. figure:: esp32-radio-clock_finish.jpg
   :width: 80%
   :align: center

I have created a radio clock using radio shield with my 
self designed `Bastelino ESP32-S2`_ base board. The clock
uses a 128x64 graphics LCD and a wooden housing. It is 
powered by an USB charge adaptor for smartphones 
and has integrated 3xAAA failsafe battery.
The device is using the `Music Box`_ software 
developed by me


.. _Bastelino ESP32-S2: https://gitlab.com/amesser-group/electronic-devices/bastelino-esp32-s2

.. _Music Box: https://gitlab.com/amesser-group/musicbox

Status
------

Radio operation and alarm clock function are finished. The hardware 
is also capable of WLAN and has SD-Card slot. However, this is not
yet implemented in music box software.


Here are some images for reference:

.. figure:: esp32-radio-clock_inner.jpg
   :width: 80%
   :align: center

.. figure:: esp32-radio-clock_assembled.jpg
   :width: 80%
   :align: center

.. figure:: esp32-radio-clock_closed.jpg
   :width: 80%
   :align: center

References
----------

.. target-notes::