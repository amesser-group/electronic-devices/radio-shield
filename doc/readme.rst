##########################
Radio Shield documentation
##########################

.. figure:: shield_variants.jpg
   :width: 400

   Different variants of the shield


The following documentation of the shield is available:

* `Comissioning of the shield <commissioning.rst>`_
* `Usage <usage.rst>`_
* `Demo Sketches documentation <demo.rst>`_
* `Errata <errata.rst>`_
* `Schematic <schematic.pdf>`_

Besides that, example usecases are shown in:

* `ESP32-S2 based radio clock <usecases/esp32-radio-clock/esp32-radio-clock.rst>`_

