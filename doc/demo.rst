***********
Usage demos
***********

Teensy Radio Console
====================

Getting Started
---------------
A demo sketch for Teensy Duino is provided in 
`TeensyRadioConsole <../../TeensyRadioConsole>`_. 
This sketch is controlled via USB Serial interface and accepts several
commands, some with arguments to control the shield.

In addition the sketch implements an USB Sound-Card which
sends all PC sounds to the RadioShields through the Teensy's
I2S interface.

Teensyduino should be configured for 48 MHz and USB with
Serial + Midi + Audio. The sketch is controlled via the 
serial monitor, "CR" line-ending. To get a list of
the commands, type "help"

In order to make a first try with radio the following
command sequence can be used:

1. Boot the Si4688 in FM mode: ``si_fm``
2. Activate the Max98089: ``max_activate``
3. Set volume on speaker: ``vol_spk 10`` or on 
   headphone/lineout ``vol_hp 5`` or even both.
   Range is 0-31. **WARNING: vol_spk >= 25 is fairly
   loud, so start with small value and increase 
   step by step**
4. Let Si4688 seek for a channel: ``seek``. Repeat command
   until a channel is found. (First invocation will just set the
   start value)

I2C Interface
-------------

The default implementation of the Wire library might not
be suffcient to properly control the SI4688 through
I2C bus. An working example I2C driver implementation is
provided in 

- `TwoWire.h <../../demo/TeensyRadioConsole/TwoWire.h>`_
- `TeensyTwi.cpp <../../demo/TeensyRadioConsole/TeensyTwi.cpp>`_

I2S Interface
-------------

The I2S Output/input provided with Teensy Audio library should not be 
used. The Audio library enables a MCLK pin which is not used
by the Radio shield but conflicts with the SD-Card interface of the
Radio Shield. A custom implementation of I2S output can
be created from the original Teensy Audio library, copying/renaming
the ``output_i2s.cpp`` and ``output_i2s.h`` files into your sketch,
renaming the classes and removing the line which enables the MCLK
clock on Pin 11. An example is provided in

- `RadioShieldI2S.h   <../../demo/TeensyRadioConsole/RadioShieldI2S.h>`_ 
- `RadioShieldI2S.cpp <../../demo/TeensyRadioConsole/RadioShieldI2S.cpp>`_ (Relevant change at end of file)


