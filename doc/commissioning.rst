****************************
Comissioning of Radio Shield
****************************

.. figure:: shield_parts.jpg

   Parts shipped with shield

Board specific steps
====================

Arduino Uno / Due / Leonardo / Mega
-----------------------------------

First Steps
^^^^^^^^^^^

You need to solder one 8-pin and one 10-pin 
header or similar sized stacking connectors to the left and
the right board edge. Then just plug the shield
onto your arduino with the antenna connector located near
the USB port.

I2S / SD-Card
^^^^^^^^^^^^^

.. attention::

   Please be aware, that you can not use the SD-Card or I2S interface 
   with any Arduino compatible board using IOREF voltage > 3.3V. The I2S
   interface can not be used with at least:

   * Arduino Uno
   * Arduino Leonardo
   * Arduino Mega 2560

Teensy V3.2 / V4.0
------------------

.. figure:: shield_wiring_back.jpg

   Overview of modifications needed for Teensy

First steps
^^^^^^^^^^^

The first step to prepare the shield for use with Teensy is to connect IOREF voltage
to 3.3V power supply. You can do this by either

* Use some solder to short pads of JP101 on the
  backside of the shield OR
* connect the 3.3V and IOREF pins of Arduino connector with a wire

.. warning::

  Do not plug the shield on an 5V Arduino after connecting IOREF to 3.3V. It will
  short the 3.3V and 5V voltages and most likely break your Arduino.

To connect Teensy V3.2 or V4.0 wth the shield, either two 14 Pin Socket or Header
strips should be soldered to the Teensy connector pins of the shield

.. attention::
   
   Please double check the correct alignment of the Teensy Pin Headers. Do not solder
   J101!

I2S Interface
^^^^^^^^^^^^^

In order to use the I2S interface with the teensy, solder 
a 4x0603 10 Ohm resistor array either to RN103 for Teensy 4.0 
or to RN104 for Teensy 3.2.

SD-Card Slot
^^^^^^^^^^^^

The SD-Card Slot can be connected to the SPI interface of Teensy board. For
that purpose you have to solder a 4x0603 10 Ohm resistor array to RN110 (the
leftmost four pads) for Teensy 3.2 or to RN106 (the rightmost four pads) for
Teensy 4.0, 



