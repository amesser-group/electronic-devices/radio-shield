#!/usr/bin/python
# encoding: utf-8

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('si4688-bypass.dat').transpose()

f = data[0]
z = data[1::2]

fig, ax = plt.subplots()


ax.loglog(f,z[0], label=u'0.068€ AN851 EMI optimized using 0402 (reference)')
ax.loglog(f,z[2], label=u'0.076€ AN851 using 0603 w/o 1µF (tested)')
ax.loglog(f,z[3], label=u'0.043€ AN851 using 0402 w/o 2.2nF (ugreen)')
ax.loglog(f,z[6], label=u'0.060€ Single 100nF Low ESL')
ax.loglog(f,z[5], label=u'0.075€ Single 470nF Low ESL')
#ax.loglog(f,z[7], label=u'0.085€ 100nF Low ESL + 1µF 0603')

ax.set(xlabel='f/Hz', ylabel='|Z|/Ohm', title='Si4688 Bypass Circuit simulations')
ax.grid(which='major', linewidth=2)
ax.grid(which='minor', linewidth=1)

ax.legend()

plt.show()
