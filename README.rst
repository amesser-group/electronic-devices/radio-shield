###################################
DAB+/FM Radio & Music shield/module
###################################

.. figure:: doc/shield_parts.jpg

Overview
========

This project is a shield/module which can be used to implement
DAB+/FM radio with Headphone/Lineout/Speaker Amplifier outputs. In addition
Audio can be fed through an I2S port to the unit and an 
Micro SD-Card connector is available.

The intention was to create a single module which can be

* a shield to turn an Arduino board into a radio 
* an extension board to be used with Teensy 3.2 & 4.0 to implement
  in addition to simple radio also a music player
* a general module in your custom configuration to be used
  as radio and audio output.

Status
======

A second, revised hardwaredesign is ready, fixing some bugs in the first one.
Testing with Arduino Uno and Teensy 3.2 is finished. Some initial work using the
Shield with `Bastelino ESP32-S2`_ has been made.

A demo software for use with Arduino/Teensyduino can be found in ``demo``
subfolder Licensing

.. _Bastelino ESP32-S2: https://gitlab.com/amesser-group/electronic-devices/bastelino-esp32-s2

Documentation
=============

Some pieces of information can be found in ``doc`` subfolder. You might start reading
with following link: `Radio Shield Documentation <doc/readme.rst>`_.

Example uses of the shield are:

 * `ESP32-S2 based radio clock <doc/usecases/esp32-radio-clock/esp32-radio-clock.rst>`_


Licensing
=========

CC BY-SA for all files in ``hardware`` subfolder

GPLv3 for all files in ``demo`` and ``scripts`` subfolder except:

* ``demo/TeensyRadioConsole/RadioShieldI2s.h``
* ``demo/TeensyRadioConsole/RadioShieldI2s.cpp``

Custom License provided as comment in the file header for 

* ``demo/TeensyRadioConsole/RadioShieldI2s.h``
* ``demo/TeensyRadioConsole/RadioShieldI2s.cpp``

Copyright
=========

For all files in ``hardware`` folder copyright is
(c) 2021 Andreas Messer <andi@bastelmap.de>.

For all files in ``demo`` subfolder the particular copyright
is provided as comment in first lines of a file itself. Holders
of copyright are:

* Paul Stoffregen <paul@pjrc.com> for files

  * ``demo/TeensyRadioConsole/RadioShieldI2s.h``
  * ``demo/TeensyRadioConsole/RadioShieldI2s.cpp``

* Andreas Messer <andi@bastelmap.de> for all remaining files

References
==========

.. target-notes::


