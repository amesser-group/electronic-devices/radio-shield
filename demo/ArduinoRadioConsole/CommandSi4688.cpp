#include "CommandSi4688.h"
#include "RadioShield.h"

const SerialCommandDefinition CommandSi4688Reset::def PROGMEM = 
{
  &(CommandSi4688Reset::activate),
  &(CommandSi4688Reset::input),
  &(CommandSi4688Reset::poll),
  "si_reset",  
};

bool
CommandSi4688Reset::activate()
{
  RadioShield.resetSi4688();
  
  return SerialIf.command_finished();
}

const SerialCommandDefinition CommandSi4688BootFM::def PROGMEM = 
{
  &(CommandSi4688BootFM::activate),
  &(CommandSi4688BootFM::input),
  &(CommandSi4688BootFM::poll),
  "si_fm",  
};

bool
CommandSi4688BootFM::activate()
{
  RadioShield.beginFMMode();
  return SerialIf.command_finished();
}

const SerialCommandDefinition CommandSi4688BootDAB::def PROGMEM = 
{
  &(CommandSi4688BootDAB::activate),
  &(CommandSi4688BootDAB::input),
  &(CommandSi4688BootDAB::poll),
  "si_dab",  
};

bool
CommandSi4688BootDAB::activate()
{
  RadioShield.beginDABMode();
  return SerialIf.command_finished();
}

const SerialCommandDefinition CommandSi4688SeekForward::def PROGMEM = 
{
  &(CommandSi4688SeekForward::activate),
  &(CommandSi4688SeekForward::input),
  &(CommandSi4688SeekForward::poll),
  "seek",  
};

bool
CommandSi4688SeekForward::activate()
{
  if(RadioShield.seekFM(false))
  {
    if(RadioShield.hasGoodSignal())
      Serial.print(F("FM Good signal: "));
    else
      Serial.print(F("FM Bad signal: "));
    
    Serial.print(RadioShield.getTunedFreq());
    Serial.print(F("kHz RSSI: "));
    
    Serial.println(RadioShield.getRssi());
  }
  else
  {
    Serial.println(F("# FM Command failed"));
  }

  return SerialIf.command_finished();
}
