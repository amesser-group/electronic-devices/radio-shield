#ifndef BASE64_H_
#define BASE64_H_

#include "Arduino.h"

class Base64Core
{
public:

protected:
  uint8_t base64_to_bits(uint8_t c);
  uint8_t bits_to_base64(uint8_t c);

};

class Base64StreamDecoder : public Base64Core
{
public:
  typedef uint8_t BufferType[3];
  
  void   start() { state = 0; }
  int8_t feed(uint8_t c);
  
  uint8_t operator [] (uint8_t idx) const { return buffer[idx]; }
  const BufferType & getBuffer()    const { return buffer; }
private:
  BufferType buffer;
  uint8_t    state;
};

class Base64StreamEncoder : public Base64Core
{
public:
  typedef uint8_t BufferType[4];
  
  void   start() { state = 0; }

  int8_t feed(uint8_t c);
  int8_t pad();
  
  uint8_t operator [] (uint8_t idx) const { return buffer[idx]; }
  const BufferType & getBuffer()    const { return buffer; }
private:
  BufferType buffer;
  uint8_t    state;
  uint8_t    bits;
};

#endif