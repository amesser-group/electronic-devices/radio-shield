#include "CommandStatus.h"
#include "RadioShield.h"

const SerialCommandDefinition CommandStatus::def PROGMEM = 
{
  &(CommandStatus::activate),
  &(CommandStatus::input),
  &(CommandStatus::poll),
  "status",
};

static const char status_unkown[]      PROGMEM = "Unkown: ";


static const char status_deviceerror[] PROGMEM = "DeviceError";
static const char status_poweron[]     PROGMEM = "PowerOn";
static const char status_shutdown[]    PROGMEM = "Shutdown";
static const char status_active[]      PROGMEM = "Active";

static const __FlashStringHelper* const max98089_statusnames[] PROGMEM =
{
  FPSTR(status_deviceerror),
  FPSTR(status_poweron),
  FPSTR(status_shutdown),
  FPSTR(status_active),
};

static const char status_standby[]     PROGMEM = "Standby";
static const char status_bootloader[]  PROGMEM = "Bootloader";

static const __FlashStringHelper* const si4688_statusnames[] PROGMEM =
{
  FPSTR(status_deviceerror),
  FPSTR(status_standby),
  FPSTR(status_poweron),
  FPSTR(status_bootloader),
};


void
CommandStatus::println_stringtable(int8_t i, int8_t i_start, const __FlashStringHelper** table, int8_t table_count)
{
  if(i < (table_count + i_start))
  {
    SerialIf.println_progmem_string_table(i, i_start, table);
  }
  else
  {
    Serial.print(FPSTR(status_unkown)); Serial.println(i);
  }
}


static const char status_deasserted[]  PROGMEM = "deasserted";
static const char status_disabled[]    PROGMEM = "disabled";
static const char status_enabled[]     PROGMEM = "enabled";

bool
CommandStatus::activate()
{
  Serial.print(F("!=== Max98089: ")); 
  println_stringtable((int8_t)RadioShield.getMax98089State(), -1, max98089_statusnames, sizeof(max98089_statusnames) / sizeof(max98089_statusnames[0]));

  Serial.print(F("!=== Si4688: "));

  auto s = RadioShield.getSi4688State();
  if(s != Si4688State::Standby)
  {
    RadioShield.waitSi4688Ready();
    SerialIf.print_hex(RadioShield.getSi4688StatusWord(), 4);
    Serial.print(' ');  
  }
  
  println_stringtable((int8_t)s, -1, si4688_statusnames, sizeof(si4688_statusnames) / sizeof(si4688_statusnames[0]));
  
  auto pin_state = RadioShield.get_pcal6408_state();

  Serial.println(F("!=== PCAL6408: "));
  Serial.print(F("! Si4688: Reset: "));
  Serial.print(FPSTR((pin_state & RadioShield.pcal6804_pinmsk_si4688_reset) ? (status_deasserted) : (status_deasserted + 2))); 
  Serial.print(F(" I2C: "));
  Serial.print(FPSTR((pin_state & RadioShield.pcal6804_pinmsk_si4688_i2cen) ? status_enabled : status_disabled)); 
  Serial.print(F(" INT: "));
  Serial.println(FPSTR((pin_state & 0x04) ? status_deasserted : (status_deasserted + 2))); 
  Serial.print(F("! Max98089: Clock: "));
  Serial.println(FPSTR((pin_state & RadioShield.pcal6804_pinmsk_max98089_clock) ? status_enabled : status_disabled)); 
  Serial.print(F("! SDCard: Plugged: "));
  Serial.print(FPSTR((pin_state & 0x20) ? status_deasserted : (status_deasserted + 2))); 
  Serial.print(F(" Power: "));
  Serial.println(FPSTR((pin_state & 0x80) ? status_disabled : status_enabled)); 
  
   
  return CommandInput::activate();
}
