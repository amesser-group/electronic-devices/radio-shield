#ifndef COMMANDSI4688_H_
#define COMMANDSI4688_H_

#include "SerialInterface.h"
#include "Base64.h"

class CommandSi4688Reset : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};

class CommandSi4688BootFM : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};


class CommandSi4688BootDAB : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};

class CommandSi4688SeekForward : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};

#endif
