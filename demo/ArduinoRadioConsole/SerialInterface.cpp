#include "Arduino.h"
#include "SerialInterface.h"

#include "CommandI2C.h"
#include "CommandStatus.h"
#include "CommandSi4688.h"
#include "CommandMax98089.h"

SerialInterface SerialInterface::inst;

static const SerialCommandDefinition* const SerialInterface::command_table[] PROGMEM =
{
  &CommandInput::def,
  &CommandHelp::def,
  &CommandStatus::def,
  &CommandSi4688Reset::def,
  &CommandSi4688BootFM::def,
  &CommandSi4688BootDAB::def,
  &CommandSi4688SeekForward::def,
  &CommandMax98089Shutdown::def,
  &CommandMax98089Activate::def,
  &CommandMax98089SpeakerVolume::def,
  &CommandMax98089HeadphoneVolume::def,
  &CommandI2C::def,
};


void
SerialInterface::begin()
{
  load_command(0);
}

void
SerialInterface::input(uint8_t c)
{
  last_input_millis = millis();
  current_command.fnInput(c);
}

void
SerialInterface::poll()
{
  current_command.fnPoll();
}

void
SerialInterface::load_command(uint8_t i)
{
  const SerialCommandDefinition* def;
  
  if(i >= (sizeof(command_table) / sizeof(command_table[0])))
    i = 1;
    
  def = pgm_read_word_near(&(command_table[i]));
  
  memcpy_P(&current_command, def, sizeof(current_command));
}

void
SerialInterface::activate_command(uint8_t i)
{
  load_command(i);
  
  if(!current_command.fnActivate())
    activate_command(1);
}

bool
SerialInterface::command_finished()
{
  Serial.print(F("% Finished after "));
  Serial.print(millis() - last_input_millis);
  Serial.println(F("ms"));

  activate_command(0);

  return true;
}


void 
SerialInterface::println_progmem_string_table(int8_t index, int8_t first_index, const __FlashStringHelper * (*progmem_string_table))
{
  const __FlashStringHelper* s;
  index = index-first_index;
  
  if(index < 0)
    return;
    
  s = pgm_read_word_near(progmem_string_table + index);
  Serial.println(FPSTR(s));
}

void 
SerialInterface::print_hex(const uint8_t* buf, uint8_t len)
{
  /* wastes one byte, don't care */
  static const char hex_map[16 + 1] PROGMEM = "0123456789ABCDEF";

  for(;len>0; --len, ++buf)
  {
    Serial.print((char)pgm_read_byte_near(&(hex_map[*buf >>  4])));
    Serial.print((char)pgm_read_byte_near(&(hex_map[*buf & 0xF])));
  }  
}


static const char i2c_transmission_nack_address[] PROGMEM = ": NACK on transmit of address";
static const char i2c_transmission_nack_data[]    PROGMEM = ": NACK on transmit of data";
static const char i2c_transmission_timeout[]      PROGMEM = ": timeout";
static const char i2c_transmission_other[]        PROGMEM = ": other";

static const __FlashStringHelper* const i2c_errornames[] PROGMEM =
{
  FPSTR(i2c_transmission_other),
  FPSTR(i2c_transmission_timeout),
  FPSTR(i2c_transmission_nack_data),
  FPSTR(i2c_transmission_nack_address),
};

int8_t
SerialInterface::printlnTwiError(uint8_t address, int8_t status)
{
  if(status < 0)
  {
    Serial.print(F("# I2C 0x"));
    print_hex(&address,1);
    SerialIf.println_progmem_string_table((int8_t)status, -4,  i2c_errornames);
  }

  return status;
}


const SerialCommandDefinition CommandInput::def PROGMEM = 
{
  &(CommandInput::activate),
  &(CommandInput::input),
  &(CommandInput::poll),
  "\0",
};

bool
CommandInput::activate()
{
  Serial.println(F("? Awaiting your command"));
  Serial.write("$ ");

  SerialIf.index = 0;
  return true;
}

void
CommandInput::input(uint8_t c)
{
  if(SerialIf.index >= (sizeof(SerialIf.buffer) - 1))
    return;
  
  if(c >= 'A' && c <= 'Z')
    c = 'a' + c - 'A';

  SerialIf.buffer[SerialIf.index] = c;
  SerialIf.index++;

  if(c == '\r')
  {
    uint8_t i;

    Serial.println(F(""));

    for(i=0; i < SerialIf.index; ++i)
    {
      if( (SerialIf.buffer[i] == '\r') ||
          (SerialIf.buffer[i] == ' ') )
      {
        SerialIf.buffer[i] = '\0';
        break;
      }
    }
    
    for(i = 1; i < (sizeof(SerialIf.command_table) / sizeof(SerialIf.command_table[0])); ++i)
    {
      const SerialCommandDefinition* def = pgm_read_word_near(&(SerialIf.command_table[i]));
      const __FlashStringHelper *cmd     = FPSTR(def->cmd);
      
      if(0 == strcmp_P((const char*)SerialIf.buffer, (const char*)cmd))
        break;
    }
    
    SerialIf.activate_command(i);
  }
  else
  {
    Serial.write(c);    
  }
}

void
CommandInput::poll()
{
  if (SerialIf.index == 0)
    return;
    
  if ((millis() - SerialIf.last_input_millis) > 10000)
  {
    Serial.println(F("\nInput timeout"));
    SerialIf.activate_command(0);  
  }
}

const SerialCommandDefinition CommandHelp::def PROGMEM = 
{
  &(CommandHelp::activate),
  &(CommandHelp::input),
  &(CommandHelp::poll),
  "help",
};

bool
CommandHelp::activate()
{
  uint8_t i;
  
  Serial.println(F("!=== Available Commands === "));
 
  for(i = 1; i < (sizeof(SerialIf.command_table) / sizeof(SerialIf.command_table[0])); ++i)
  {
    const SerialCommandDefinition* def = pgm_read_word_near(&(SerialIf.command_table[i]));
    const __FlashStringHelper *cmd     = FPSTR(def->cmd);
    
    Serial.write("!  ");
    Serial.println(cmd);
  }

  return CommandInput::activate();
}
