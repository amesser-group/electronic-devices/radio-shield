#ifndef COMMANDMAX98089_H_
#define COMMANDMAX98089_H_

#include "SerialInterface.h"

class CommandMax98089Shutdown : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};

class CommandMax98089Activate : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};


class CommandMax98089HeadphoneVolume : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};

class CommandMax98089SpeakerVolume : public CommandInput
{
public:  
  static const SerialCommandDefinition def;
private:
  static bool activate();  
};

#endif
