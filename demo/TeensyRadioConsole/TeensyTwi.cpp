#include "TwoWire.h"
#include "SerialInterface.h"


#if !defined(__MK20DX256__)
#error Currently only Teensy 3.2 is supported
#endif

struct __attribute__((packed)) K20I2CRegisters
{
  volatile uint8_t  A1;
  volatile uint8_t  F;
  volatile uint8_t  C1;
  volatile uint8_t  S;
  volatile uint8_t  D;
  volatile uint8_t  C2;
  volatile uint8_t  FLT;
  volatile uint8_t  RA;
  volatile uint8_t  SMB;
  volatile uint8_t  A2;
  volatile uint8_t  SLTH;
  volatile uint8_t  SLTL;
};

static K20I2CRegisters & I2C0 = *reinterpret_cast<K20I2CRegisters*>(0x40066000);

template<unsigned long divider>
struct K20I2CDividerTable : public K20I2CDividerTable<divider +1>{};

template<> struct K20I2CDividerTable<1 * 20>  { static constexpr uint8_t F_Value = 0x00 | 0x00; };
template<> struct K20I2CDividerTable<2 * 20>  { static constexpr uint8_t F_Value = 0x40 | 0x00; };
template<> struct K20I2CDividerTable<4 * 20>  { static constexpr uint8_t F_Value = 0x80 | 0x00; };
template<> struct K20I2CDividerTable<1 * 22>  { static constexpr uint8_t F_Value = 0x00 | 0x01; };
template<> struct K20I2CDividerTable<2 * 22>  { static constexpr uint8_t F_Value = 0x40 | 0x01; };
template<> struct K20I2CDividerTable<4 * 22>  { static constexpr uint8_t F_Value = 0x80 | 0x01; };
template<> struct K20I2CDividerTable<1 * 24>  { static constexpr uint8_t F_Value = 0x00 | 0x02; };
template<> struct K20I2CDividerTable<2 * 24>  { static constexpr uint8_t F_Value = 0x40 | 0x02; };
template<> struct K20I2CDividerTable<4 * 24>  { static constexpr uint8_t F_Value = 0x80 | 0x02; };
template<> struct K20I2CDividerTable<1 * 26>  { static constexpr uint8_t F_Value = 0x00 | 0x03; };
template<> struct K20I2CDividerTable<2 * 26>  { static constexpr uint8_t F_Value = 0x40 | 0x03; };
template<> struct K20I2CDividerTable<4 * 26>  { static constexpr uint8_t F_Value = 0x80 | 0x03; };
template<> struct K20I2CDividerTable<1 * 64>  { static constexpr uint8_t F_Value = 0x00 | 0x12; };
//template<> struct K20I2CDividerTable<2 * 64>  { static constexpr uint8_t F_Value = 0x40 | 0x12; };
//template<> struct K20I2CDividerTable<4 * 64>  { static constexpr uint8_t F_Value = 0x80 | 0x12; };
template<> struct K20I2CDividerTable<1 * 128> { static constexpr uint8_t F_Value = 0x00 | 0x1B; };
template<> struct K20I2CDividerTable<2 * 128> { static constexpr uint8_t F_Value = 0x40 | 0x1B; };
template<> struct K20I2CDividerTable<4 * 128> { static constexpr uint8_t F_Value = 0x80 | 0x1B; };

/* for 72 Mhz CPU Cfreq */
template<> struct K20I2CDividerTable<4 * 192> { static constexpr uint8_t F_Value = 0x80 | 0x1E; };
/* for 96 Mhz CPU Cfreq */
template<> struct K20I2CDividerTable<4 * 240> { static constexpr uint8_t F_Value = 0x80 | 0x1F; };




TeensyTwi TeensyTwi::inst;

void
TeensyTwi::begin()
{
  constexpr unsigned long divider = F_CPU / 100000;
  
  SIM_SCGC4 |= SIM_SCGC4_I2C0;

  configurePins();

  I2C0.F   = K20I2CDividerTable<divider>::F_Value;
  I2C0.FLT = 100000 / 12000000;
  I2C0.C2  = I2C_C2_HDRS;
  I2C0.C1  = I2C_C1_IICEN;

  if(!waitBusNotBusy())
    resetBus();
}

void
TeensyTwi::configurePins()
{
  volatile uint32_t* pcr;

  pcr = portConfigRegister(18);
  *pcr = (PORT_PCR_MUX(2)|PORT_PCR_ODE|PORT_PCR_SRE|PORT_PCR_DSE);

  pcr = portConfigRegister(19);
  *pcr = (PORT_PCR_MUX(2)|PORT_PCR_ODE|PORT_PCR_SRE|PORT_PCR_DSE);
}

void
TeensyTwi::resetBus()
{
  Serial.printf("tw bus reset: C1: 0x%02X S:0x%02X\r\n", I2C0.C1, I2C0.S);
  
  pinMode(18, INPUT);
  digitalWrite(19,HIGH);
  pinMode(19,OUTPUT);

  for(uint8_t count=0; digitalRead(18) == 0 && count < 9; count++)
  {
      digitalWrite(19,LOW);
      delayMicroseconds(5);       // 10us period == 100kHz
      digitalWrite(19,HIGH);
      delayMicroseconds(5);
  }

  configurePins();

  I2C0.C1 = 0;
  delayMicroseconds(5);
  Serial.printf("tw busy timeout: C1: 0x%02X S:0x%02X\r\n", I2C0.C1, I2C0.S);
  I2C0.C1 = I2C_C1_IICEN;
  delayMicroseconds(5);
  Serial.printf("tw busy timeout: C1: 0x%02X S:0x%02X\r\n", I2C0.C1, I2C0.S);  
}



bool
TeensyTwi::waitProcessing()
{
  int8_t   tmp = ~remaining_bytes;
  uint32_t ts_timeout;

  ts_timeout = micros() + timeout_micros;
  while((I2C0.S & I2C_S_IICIF) == 0)
  {
    /*
    if (tmp != remaining_bytes)
      ts_timeout = micros() + timeout_micros;
    else*/if ( (int32_t)(ts_timeout - micros()) < 0)
      return false;
  }  

  return true;
}

bool
TeensyTwi::waitBusNotBusy()
{
  uint32_t ts_timeout = micros() + timeout_micros;
  while((I2C0.S & I2C_S_BUSY))
  {
    if ( (int32_t)(ts_timeout - micros()) < 0)
    {
      Serial.printf("tw: busy timeout \r\n");
     return false;
    }
  }

  return true;
}

int8_t TeensyTwi::start(uint8_t val)
{
  slarw           = val;
  remaining_bytes = 0;

  // Serial.print("tw pre-start: "); Serial.println(val); 

  I2C0.S = I2C_S_ARBL | I2C_S_IICIF;

  if(I2C0.C1 & I2C_C1_MST)
  {
    I2C0.C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_RSTA | I2C_C1_TX;
  }
  else
  {
    if(!waitBusNotBusy())
    {
      resetBus();
      return -3;
    }  
    
    I2C0.C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_TX;
  }

  if((I2C0.C1 & I2C_C1_MST) == 0)
    return -4;

  // Serial.println("tw acquired bus");
  
  I2C0.D = slarw;

  if(!waitProcessing())
  {
    I2C0.C1 = I2C_C1_IICEN;
    return -3;
  }

  I2C0.S = I2C_S_IICIF;

  if(I2C0.S & I2C_S_ARBL)
  {
    I2C0.C1 = I2C_C1_IICEN;
    return -4;
  }

  if(I2C0.S & I2C_S_RXAK)
  {
    Serial.println("tw address nack");
    I2C0.C1 = I2C_C1_IICEN;
    return -1;
  }

  if (slarw & 0x1)
  {
    uint8_t dummy;
    I2C0.C1 = I2C_C1_IICEN | I2C_C1_MST;
    dummy = I2C0.D;
  }

  return 0;
}

void
TeensyTwi::endTransfer()
{
  if((I2C0.C1 & I2C_C1_MST) == 0)
    return;

  waitProcessing();

  if((I2C0.C1 & I2C_C1_TX) == 0)
  {
    uint8_t dummy;
    // Serial.print(F("tw stop with nak: \r\n"));
    
    I2C0.C1 = I2C_C1_IICEN | I2C_C1_MST | I2C_C1_TXAK;
    I2C0.S = I2C_S_IICIF;

    dummy = I2C0.D;
  }
  else
  {
    // Serial.print(F("tw stop without nak\r\n"));
  }

  waitProcessing();
  
  I2C0.C1 = I2C_C1_IICEN;

  waitBusNotBusy();

  // Serial.printf("tw transfer ended: C1: 0x%02X C2: 0x%02X S:0x%02X\r\n", I2C0.C1, I2C0.C2, I2C0.S);  
}

int8_t
TeensyTwi::transmit(const uint8_t *data, int8_t data_len)
{
  transmit_buffer  = data;
  remaining_bytes  = data_len;
  
  if(remaining_bytes == 0)
    return 0;

  // Serial.println("tw: transmitting data");
  do
  {
    I2C0.S |= I2C_S_IICIF;

    I2C0.D = *(transmit_buffer++);
    remaining_bytes--;

    if(!waitProcessing())
      return -3;

    if(I2C0.S & I2C_S_ARBL)
      return -4;

    if(I2C0.S & I2C_S_RXAK)
      return -2;
  } while(remaining_bytes > 0);

  return data_len - remaining_bytes;
}

int8_t
TeensyTwi::receive(uint8_t *data, int8_t data_len)
{
  receive_buffer   = data;
  remaining_bytes  = data_len;

  if(remaining_bytes == 0)
    return 0;

  // Serial.println("tw: receiving data");
  do
  {
    if(!waitProcessing())
      return -3;

    I2C0.S = I2C_S_IICIF;

    *(receive_buffer++) = I2C0.D;
    remaining_bytes--;
    
  } while(remaining_bytes > 0);

  return data_len - remaining_bytes;
  
}
