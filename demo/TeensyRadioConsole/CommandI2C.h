#ifndef COMMANDI2C_H_
#define COMMANDI2C_H_

#include "SerialInterface.h"
#include "Base64.h"

class CommandI2C : public CommandInput
{
public:
  static const SerialCommandDefinition def;
private:
  static bool activate();
  static void input(uint8_t c);
  static void poll();

  int8_t do_write(bool stop);

  enum class State : uint8_t
  {
    WRITE_W_DATA  = 0,
    WRITE_PENDING,
    READING
  };
  
  uint32_t len_to_read;
  uint8_t  addr;
  State    state;

  union {
    Base64StreamDecoder decoder;
    Base64StreamEncoder encoder;
  } b64;
  
  static CommandI2C inst;
};

#endif
