#include "Base64.h"

uint8_t
Base64Core::base64_to_bits(uint8_t c)
{
  // Capital letters - 'A' is ascii 65 and base64 0
  if('A' <= c && c <= 'Z') return c - 'A';
  
  // Lowercase letters - 'a' is ascii 97 and base64 26
  if('a' <= c && c <= 'z') return c - 71;
  
  // Digits - '0' is ascii 48 and base64 52
  if('0' <= c && c <= '9') return c + 4;
  
  // '+' is ascii 43 and base64 62
  if(c == '+') return 62;
  
  // '/' is ascii 47 and base64 63
  if(c == '/') return 63;

  return 64;
}

uint8_t
Base64Core::bits_to_base64(uint8_t c)
{
  c &= 0x3F;
  
  if(c <= ('Z' - 'A'))
    return 'A' + c;
    
  c -= ('Z' - 'A') + 1;

  if(c <= ('z' - 'a'))
    return 'a' + c;

  c -= ('z' - 'a') + 1;

  if(c <= ('9' - '0'))
    return '0' + c;

  c -= ('9' - '0') + 1;
  
  if (c == 0)
    return '+';
  else if ( c == 1)
    return '/';
  else
    return '?';
}


int8_t
Base64StreamDecoder::feed(uint8_t c)
{
  uint8_t bits = base64_to_bits(c);
  
  bits = bits & 0x3F;
  switch(state & 0x03)
  {
    case 0:
      buffer[0]  = (bits << 2); 
      state = 1;
      break;
    case 1: 
      buffer[0] |= (bits >> 4); 
      buffer[1]  = (bits << 4);
      state++;
      break;
    case 2: 
      buffer[1] |= (bits >> 2); 
      buffer[2]  = (bits << 6);
      state++;
      break;
    case 3: 
      buffer[2] |= bits;
      state++;
      break;
  }

  /* remind first padding byte added */
  if ( c == '=' && (state & 0xF0) == 0)
    state = state | (state << 4);
    
  if ( state == 4)
    return 3;
  if ( (state & 0xF) == 4)
    return (state >> 4) - 2;
  else
    return 0;
}


int8_t
Base64StreamEncoder::feed(uint8_t c)
{
  switch(state & 0x3)
  {
    case 0:
      buffer[0] = bits_to_base64(c >> 2);
      bits      = c << 4;
      state     = 1;
      break;
    case 1:
      buffer[1] = bits_to_base64(bits | (c >> 4));
      bits      = c << 2;
      state     = 2;
      break;
    case 2:
      buffer[2] = bits_to_base64(bits | (c >> 6));
      buffer[3] = bits_to_base64(c);
      state     = 4;
      break;
  }
  
  if(state == 4)
    return 4;
  else
    return 0;
}

int8_t
Base64StreamEncoder::pad()
{
  switch(state)
  {
  case 1:
    buffer[1] = bits_to_base64(bits);
    buffer[2] = '=';
    buffer[3] = '=';
    state = 4;
    break;
  case 2:
    buffer[2] = bits_to_base64(bits);
    buffer[3] = '=';
    state = 4;
    break;
  }
  
  if(state == 4)
    return 4;
  else
    return 0;
}
