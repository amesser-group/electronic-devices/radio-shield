#include "CommandI2C.h"
#include "TwoWire.h"

CommandI2C CommandI2C::inst;

const SerialCommandDefinition CommandI2C::def PROGMEM = 
{
  &(CommandI2C::activate),
  &(CommandI2C::input),
  &(CommandI2C::poll),
  "i2c",
};

bool
CommandI2C::activate()
{
  char *s,*q;
  unsigned long val;
  
  /* ensure null terminatation */
  s = SerialIf.buffer;
  s[SerialIf.index] = '\0';
  
  s +=4;
  val = strtoul(s, &q, 0);
  
  if(q == s)
    return false;
  else if (val > 127)
    return false;
    
  inst.addr = val;
  for(s = q; *s != '\0'; ++s)
    if(*s != ' ')
      break;
      
  inst.len_to_read = strtoul(s, &q, 0);
  if(q == s)
    return false;
    
  inst.state = State::WRITE_W_DATA;

  Serial.write("!=== Starting I2C transfer with ");
  Serial.println(inst.addr);
  
  inst.b64.decoder.start();
  
  TwoWire.beginWrite(inst.addr);
  return true;
}


void 
CommandI2C::input(uint8_t c)
{
  if(c == '\r')
  {
    Serial.println(F(""));
    
    if (inst.len_to_read > 0)
    {
      inst.b64.encoder.start();
      inst.state = State::READING;
      TwoWire.beginRead(inst.addr);
      poll();
    }
    else
    {
      TwoWire.endTransfer();
      SerialIf.command_finished();
    }
  }
  else if (inst.state != State::READING)
  {
    int8_t n;

    Serial.write(c);
          
    n = inst.b64.decoder.feed(c);
    if(n > 0)
    {
      int r;

      r = TwoWire.transmit(inst.b64.decoder.getBuffer(), n);

      if( (r >= 0) && (r != n))
        TwoWire.endTransfer();

      if(r != n)
      {
        SerialIf.printlnTwiError(r);
        SerialIf.command_finished();
        return;
      }
    }
  }
}

void 
CommandI2C::poll()
{
  uint8_t buffer[3];
  int8_t i,r;
  
  if(inst.state != State::READING)
  {
    CommandInput::poll();
    return;
  }

  if(inst.len_to_read > 3)
    r = TwoWire.receive(buffer, 3);
  else
    r = TwoWire.receive(buffer, inst.len_to_read);
    
  if(r <= 0)
  {
    Serial.println(F("#"));
    SerialIf.command_finished();
    return;
  }
  
  inst.len_to_read -= r;
  
  for(i = 0; i < r; ++i)
    inst.b64.encoder.feed(buffer[i]);
  
  if( 4 == inst.b64.encoder.pad())
  {
    Serial.write((const char*)&(inst.b64.encoder.getBuffer()), 4);
  }
  else
  {
    TwoWire.endTransfer();
    Serial.println(F("#"));
    SerialIf.command_finished();
    return;
  }
  
  if(inst.len_to_read == 0)
  {
    TwoWire.endTransfer();
    Serial.println(F(""));
    SerialIf.command_finished();
  }
}
