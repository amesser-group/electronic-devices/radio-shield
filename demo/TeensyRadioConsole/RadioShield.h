#ifndef RADIOSHIELD_H_
#define RADIOSHIELD_H_

#include <stdint.h>

#include "Si4688Defs.h"
#include "Max98089Defs.h"

enum class Max98089State : int8_t 
{
  DeviceError = -1,
  PowerOn     = 0,
  Shutdown,
  Active,
};

enum class Si4688State : int8_t 
{
  DeviceError = -1,
  Standby     = 0,
  PowerOn,
  Bootloader,
  Firmware,
  Unkown = 127
};

typedef uint8_t Si4688StatusWord[4];

class RadioShieldImplementation
{
public:
  void begin();
  
  bool begin_max98089();

  bool shutdownMax98089();
  bool activateMax98089();
  bool setSpeakerVolume(uint8_t vol);
  bool setHeadphoneVolume(uint8_t vol);

  bool shutdownSi4688();
  bool resetSi4688();
  bool waitSi4688Ready();
  bool doSi4688BootFirmware(uint32_t flash_address);

  
  bool beginDABMode();
  bool beginFMMode();
  bool seekFM(bool backward);
  
  bool isReady() const { return shield_ready != 0;}

  constexpr Max98089State getMax98089State() const { return max98089_state;}
  Si4688State getSi4688State() const;
  const Si4688StatusWord & getSi4688StatusWord() const { return si4688_status; }

  uint32_t getTunedFreq()   const {return freq;}
  uint8_t  getRssi()        const {return rssi;}
  bool     hasGoodSignal()  const {return signal_good;}
  
private:
  static int8_t i2c_write_read(uint8_t address, uint8_t *buf, int8_t write_len, int8_t read_len);
  static int8_t i2c_write_P   (uint8_t address, const uint8_t *write_buf, int16_t write_len);
  
  bool begin_pcal6408();
  bool update_pcal6408();
  
  bool enable_max98089_clock();

  bool doSi4688LoadInit();
  bool setSi4688Property(uint16_t property, uint16_t value);
  bool setSi4688Properties_P(const uint16_t *props);
  
  uint8_t shield_ready;
  uint8_t       pcal6408_configreg;
  
  Max98089State    max98089_state;
  Si4688StatusWord si4688_status;

  uint32_t freq;
  uint8_t  rssi;
  bool     signal_good;

  static constexpr int8_t  i2caddr_max98089 = 0x10;
  static constexpr int8_t  i2caddr_pcal6408 = 0x20;
  static constexpr int8_t  i2caddr_si4688   = 0x64;
  
  
  static constexpr uint8_t pcal6804_pinmsk_si4688_i2cen   = 0x01;
  static constexpr uint8_t pcal6804_pinmsk_si4688_reset   = 0x02;
  static constexpr uint8_t pcal6804_pinmsk_max98089_clock = 0x40;
  
};

extern RadioShieldImplementation RadioShield;

#endif
