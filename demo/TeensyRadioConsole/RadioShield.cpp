#include "RadioShield.h"
#include "SerialInterface.h"
#include "TwoWire.h"


#include <avr/pgmspace.h>

RadioShieldImplementation RadioShield;

template<int n>
class CommandBuffer
{
public:
  void setField8(uint8_t idx,  uint8_t value)  { buffer[idx] = value; }
  void setField16(uint8_t idx, uint16_t value) { buffer[idx] = value & 0xFF; buffer[idx + 1] = (value >> 8) & 0xFF; }
  void setField32(uint8_t idx, uint32_t value) { setField16(idx, value & 0xFFFF); setField16(idx + 2, (value >>  16) & 0xFFFF);}

  constexpr uint8_t  getField8 (uint8_t idx) const { return buffer[idx];}
  constexpr uint16_t getField16(uint8_t idx) const { return buffer[idx] | ((uint16_t)buffer[idx + 1] << 8);}
  constexpr uint32_t getField32(uint8_t idx) const { return getField16(idx) | (((uint32_t)getField16(idx + 2)) << 16);}
  
  uint8_t buffer[n];
};

void
RadioShieldImplementation::begin()
{
  shield_ready = false;

  if(!begin_pcal6408())
    return;
    
  if(!begin_max98089())
    return;
    
  shield_ready = true;
}

int8_t 
RadioShieldImplementation::i2c_write_read(uint8_t address, uint8_t *buf, int8_t write_len, int8_t read_len)
{
  int8_t w,r;

  w = 0;
  if(write_len > 0)
  {
    w = TwoWire.beginWrite(address);

    if(w < 0)
      return SerialIf.printlnTwiError(w);

    w = TwoWire.transmit(buf, write_len);

    if(w < 0)
      return SerialIf.printlnTwiError(w);
  }

  r = 0;
  if((w == write_len) && (read_len > 0))
  {
    r = TwoWire.beginRead(address);

    if(r < 0)
      return SerialIf.printlnTwiError(r);

    r = TwoWire.receive(buf, read_len);

    if(r < 0)
      return SerialIf.printlnTwiError(r);
  }

  TwoWire.endTransfer();
  return r;
}
  
int8_t 
RadioShieldImplementation::i2c_write_P(uint8_t address, const uint8_t *write_buf, int16_t write_len)
{
  int16_t w;

  w = TwoWire.beginWrite(address);

  if(w < 0)
    return SerialIf.printlnTwiError(w);

  for(w = 0; w < write_len; ++w)
  {
    uint8_t buf = pgm_read_byte_near(&(write_buf[w]));
    int8_t i;
    
    i = TwoWire.transmit(&buf, 1);
      
    if(i < 0)
      return SerialIf.printlnTwiError(i);
    else if (i != 1)
      break;
  }

  TwoWire.endTransfer();
  return 0;
}


bool
RadioShieldImplementation::begin_pcal6408() 
{
  uint8_t buf[2];

  /* enable pull ups as required */
  buf[0] = 0x43; buf[1] = 0x7C;
  if (i2c_write_read(i2caddr_pcal6408, buf, 2, 0) != 0)
    return false;

  /* pre-configure output pin states */
  buf[0] = 0x01; buf[1] = 0b00011011;
  if (i2c_write_read(i2caddr_pcal6408, buf, 2, 0) != 0)
    return false;

  /* configure output drivers. This will disconnect & reset si4688 & disable the max98089 clock oscillator */
  pcal6408_configreg = 0b10111111;
  if(!update_pcal6408())
    return false;

  /* read input register */
  buf[0] = 0;
  if (i2c_write_read(i2caddr_pcal6408, buf, 1, 1) != 1)
    return false;
    
  /* Serial.write("PCAL6408 Status: "); serial_dump_hex(buf,2); Serial.write('\n'); */

  /* Check if all lines are in expected state,
   * ignore sd-card detect pin */
  if( (buf[0] | 0x20) != 0xBC)
    return false;

  return true;  
}

bool
RadioShieldImplementation::update_pcal6408()
{
  uint8_t buf[2] = {0x3, pcal6408_configreg};

  if (i2c_write_read(i2caddr_pcal6408, buf, 2, 0) != 0)
    return false;

  return true;  
}


bool
RadioShieldImplementation::enable_max98089_clock()
{
  pcal6408_configreg |= pcal6804_pinmsk_max98089_clock;
  
  if(!update_pcal6408())
    return false;
    
  delay(4);
  
  return true;
}
