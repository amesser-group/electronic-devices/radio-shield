#ifndef SI4688_DEFS_H_
#define SI4688_DEFS_H_

#include <stdint.h>

enum SI4688_CMD : uint8_t
{
  CMD_RD_REPLY                 = 0x00,
  CMD_POWER_UP                 = 0x01,
  CMD_HOST_LOAD                = 0x04,
  CMD_FLASH_PASS_THROUGH       = 0x05,
  CMD_LOAD_INIT                = 0x06,
  CMD_BOOT                     = 0x07,
  CMD_GET_SYS_STATE            = 0x09,
  CMD_SET_PROPERTY             = 0x13,

  CMD_FM_TUNE_FREQ             = 0x30,
  CMD_FM_SEEK_START            = 0x31,
  CMD_FM_RSQ_STATUS            = 0x32,
  CMD_FM_RDS_STATUS            = 0x34,
  CMD_FM_RDS_BLOCKCOUNT        = 0x35,

  CMD_GET_DIGITAL_SERVICE_LIST = 0x80,
  CMD_START_DIGITAL_SERVICE    = 0x81,
  CMD_DAB_TUNE_FREQ            = 0xB0,
  CMD_DAB_DIGRAD_STATUS        = 0xB2,
  CMD_DAB_GET_EVENT_STATUS     = 0xB3,
  CMD_DAB_SET_FREQ_LIST        = 0xB8,

  CMD_TEST_GET_RSSI            = 0xE5,
};

enum SI4688_FM_SEEK_START_ARG2_MSK
{
  MSK_WRAP       = 0x01,
  MSK_SEEKUP     = 0x02,
};
  
enum SI4688_PROP : uint16_t
{
  PROP_DIGITAL_IO_OUTPUT_SELECT                  = 0x0200,
  PROP_DIGITAL_IO_OUTPUT_SAMPLE_RATE             = 0x0201,
  PROP_DIGITAL_IO_OUTPUT_FORMAT                  = 0x0202,
  PROP_PIN_CONFIG_ENABLE                         = 0x0800,

  PROP_TUNE_FE_VARM                              = 0x1710,
  PROP_TUNE_FE_VARB                              = 0x1711,
  PROP_FM_TUNE_FE_CFG                            = 0x1712,
  PROP_DAB_TUNE_FE_CFG                           = 0x1712,


  PROP_FM_RDS_CONFIG                             = 0x3C02,

  PROP_DAB_VALID_RSSI_TIME                       = 0xB200,
  PROP_DAB_VALID_SYNC_TIME                       = 0xB203,
  PROP_DAB_VALID_DETECT_TIME                     = 0xB204,
  PROP_DAB_ACF_ENABLE                            = 0xB500,
  PROP_DAB_CTRL_DAB_MUTE_SIGNAL_LEVEL_THRESHOLD  = 0xB501,
  PROB_DAB_ANOUNCEMENTS                          = 0xB700,
};

enum SI4688_STATUS0_MSK : uint8_t
{
  MSK_STATUS0_CTS    = 0x80,
  MSK_STATUS0_ERR    = 0x40,
  MSK_STATUS0_RSQINT = 0x08,
  MSK_STATUS0_STCINT = 0x01,
};

#endif
